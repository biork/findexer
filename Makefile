
INSTALL_DIR=/local/bin
TARGET=index

all : $(TARGET)

CFLAGS += -Wall 

ifdef DEBUG
CFLAGS += -g -D_DEBUG
else
CFLAGS += -O3 -DNDEBUG
endif

$(TARGET) : main.c
	$(CC) $(CFLAGS) -o $@ $^

install : $(TARGET)
	install $(TARGET) $(INSTALL_DIR)

clean :
	rm -f $(TARGET) *.ii *.o *.s


