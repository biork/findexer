
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <errno.h>  // for perror
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <setjmp.h>

#include <alloca.h>
#include <fcntl.h>
#include <sys/mman.h>

#define __max( a, b ) ( (a)<(b) ? (b) : (a) )

/**
  * MAXLEN_KEY 
  * Performance will decrease with increasing key size for multiple reasons.
  * Generally, however, performance is orders of magnitude better than the
  * alternative of linear searching, and the code is significantly more
  * complex if dynamically-sized keys are allowed, so... For now
  * maximum key size is fixed at compile time to be large enough to accomo-
  * date the largest accession id I know of (in bioinformatics): Ensembl's.
  * Revisit dynamic keys later.
  */
#define MAXLEN_KEY (18)  // Chosen to accomodate Ensembl's loooong ids.

static jmp_buf context;

static int arg_verbosity = 1;
#define VERB_SILENT (0)
#define VERB_INFO   (1)
#define VERB_DEBUG  (2)

static bool arg_optimize     = false;
static bool arg_numeric_keys = false; // default because strings ALWAYS work
static bool arg_emit_empty_lines = true;
static int  arg_column = 1;
static int  arg_skip = 0;
static int  arg_n_lines = 0;

static char arg_fifoname[ FILENAME_MAX + 1 ];
static const char *NAMED_PIPE = "INDEXFIFO";

/**
  * This indexer can index on string or integer keys...
  */
typedef struct {
	int  key;
	long offset;
} numkey_t;

typedef struct {
	char key[MAXLEN_KEY+1];
	long offset;
} strkey_t;

/**
  * ...but only on one type per target file.
  */
static union {
	numkey_t *num;
	strkey_t *str;
} g_index;

static bool g_one_to_one = true; // until one of __cmp_'s show otherwise

/**
  * The maximum number of requests the server can accept in one batch.
  * This is dynamically (and monotonically!) increased if necessary.
  */
static int g_request_buf_len = 512;


/**
  * Comparators for all occasions.
  * The first two are used by qsort calls when building indices.
  * They implicitly update a global that flags non-injective keys.
  * They third is used to sort file offsets to optimize retrievals.
  */

static int __cmp_int_keys( const void *pvl, const void *pvr ) {
	const numkey_t *pl = (const numkey_t *)pvl;
	const numkey_t *pr = (const numkey_t *)pvr;
	const int diff = pl->key - pr->key;
	g_one_to_one = g_one_to_one and (diff != 0);
	return diff;
}


static int __cmp_str_keys( const void *pvl, const void *pvr ) {
	const strkey_t *pl = (const strkey_t *)pvl;
	const strkey_t *pr = (const strkey_t *)pvr;
	const int diff = strncmp( pl->key, pr->key, MAXLEN_KEY );
#ifdef _DEBUG
#warning "Serious slow down"
	if( 0 == diff ) {
		fprintf( stderr, "%s == %s\n", pl->key, pr->key );
		fflush(stderr);
	}
#endif
	g_one_to_one = g_one_to_one and (diff != 0);
	return diff;
}


static int __cmp_foffset( const void *pvl, const void *pvr ) {
	const long *pl = (const long *)pvl;
	const long *pr = (const long *)pvr;
	return *pl - *pr;
}


static int mkindex( 
		FILE *fp, 
		int skip, 
		int key_column,
		const int COUNT ) {

	int ilen = 0;
	char lc, cc = 0; // last-char, current-char

	/**
	  * Allocate a working buffer for identifiers.
	  */
	char *keybuf = (char *)alloca(MAXLEN_KEY+1);

	/**
	  * Skip 'skip' lines. This is the most general-purpose way
	  * of skipping header and comment lines.
	  */
	while( skip-- > 0 and not feof(fp) ) {
		do { cc = fgetc( fp ); } while( EOF != cc and '\n' != cc );
	}

	/**
	  * Extract the key from every line.
	  */
	lc = '\n'; // ...so that leading whitespace is skipped.
	while( EOF != cc ) {

		/**
		  * Note the file offset at the start of the current line.
		  */
		long fpos = ftell( fp );
		int flen = 0;
		int cnum = 0;
		
		/**
		  * Find target column by counting space-to-nonspace transitions.
		  */

		do {
			cc = fgetc( fp );
			if( isspace(lc) and isgraph(cc) ) cnum += 1;
			lc = cc;
			if( cnum == key_column ) {
				if( isgraph(cc) ) { 
					if( flen < MAXLEN_KEY ) {
						keybuf[flen++] = cc; 
					} else {
						fprintf( stderr, "key too long around line %d\n", ilen );
					}
				} else {

					if( not ( ilen < COUNT )) {
						longjmp( context, __LINE__ );
					}

					/**
					  * The preceding check would seem to make more sense
					  * above and outside this loop. Must be where it is to
					  * allow fgetc to hit the EOF that triggers loop exit.
					  */

					keybuf[flen] = 0; 
					if( arg_numeric_keys ) {
						numkey_t *pk = g_index.num + ilen;
						pk->key    = (int)strtol( keybuf, NULL, 0 );
						pk->offset = fpos;
					} else {
						strkey_t *pk = g_index.str + ilen;
						strncpy( pk->key, keybuf, MAXLEN_KEY );
						pk->offset = fpos;
					}
					ilen++;
					break;
				}
			}
		} while( EOF != cc );

		/**
		  * ...and skip the remainder of the line.
		  * Note that the indexed column -could- be the last, so we might
		  * have already reached the end of the line.
		  */
		while( EOF != cc and '\n' != cc ) cc = fgetc( fp );
		lc = cc;
	}

	/**
	  * Sort the index, and check for duplicates. Key's need not be unique
	  * but if they are, retrieval is slightly faster...
	  */

	if( arg_numeric_keys ) {
		qsort( g_index.num, ilen, sizeof(numkey_t), __cmp_int_keys );
#if 0
		int i = ilen-1;
		while( i-- > 0 ) {
			if( g_index.num[i].key == g_index.num[i+1].key ) {
				g_one_to_one = false;
				break;
			}
		}
#endif
	} else {
		qsort( g_index.str, ilen, sizeof(strkey_t), __cmp_str_keys );
#if 0
		int i = ilen-1;
		while( i-- > 0 ) {
			if( strncmp( g_index.str[i].key, g_index.str[i+1].key, MAXLEN_KEY ) == 0 ) {
				g_one_to_one = false;
				break;
			}
		}
#endif
	}
	return ilen;
}


/**
  * These retrieval functions return half-open ranges, [from, to),
  * of index indices that match the given key. These must still be
  * converted to file offsets.
  */
static int lookup_num( int key, const int L, int *top ) {
	int lo = 0;
	int hi = L;
	while( lo < hi ) {
		int m = (lo+hi)/2;
		if( g_index.num[m].key < key )
			lo = m+1;
		else
			hi = m;
	}
	// If we found -any- matches...
	if( lo < L and g_index.num[lo].key == key ) {
		// ...we might have to search for more...
		if( g_one_to_one ) {
			*top = lo+1;
		} else { // find the half-open bounds...
			assert( lo == hi );
			while(   lo > 0 
				and g_index.num[lo-1].key == key ) --lo;
			while( ++hi < L 
				and g_index.num[hi  ].key == key )     ;
			*top = hi;
		}
		return lo;
	} else
		return -1; // ...and leave top alone, caller initialized it!
}


static int lookup_str( const char *key, const int L, int *top ) {
	int lo = 0;
	int hi = L;
	while( lo < hi ) {
		int m = (lo+hi)/2;
		if( strcmp( g_index.str[m].key, key ) < 0 )
			lo = m+1;
		else
			hi = m;
	}
	// If we found -any- matches...
	if( lo < L and strcmp( g_index.str[lo].key, key ) == 0 ) {
		// ...we might have to search for more...
		if( g_one_to_one ) {
			*top = lo+1;
		} else { // find the half-open bounds...
			assert( lo == hi );
			while(   lo > 0 
				and strcmp( g_index.str[lo-1].key, key ) == 0 ) --lo;
			while( ++hi < L 
				and strcmp( g_index.str[hi  ].key, key ) == 0 )     ;
			*top = hi;
		}
		return lo;
	} else
		return -1; // ...and leave top alone, caller initialized it!
}


static int serve( 
		const char *filename, 
		const char *fifoname,
		int index_len,
	   	bool opt_output_empty_lines ) {

	bool eot = false;
	struct stat info;
	char *buf = (char *)alloca(MAXLEN_KEY+2); // fgets appends \n AND \0 !
	FILE *fp = NULL;
	long *offsets;
	int fd, n_req = 0;
	const char *base;

	/**
	  * Open and memory map the indexed file.
	  */

	fd = open( filename, O_RDONLY );
	if( fd < 0 ) {
		perror( "failed opening target file" );
		return -1;
	}

	fstat( fd, &info );
	if( info.st_size == 0 ) {
		close( fd );
		return -1;
	}

	base = (const char*)mmap( NULL, 
			info.st_size, PROT_READ, MAP_SHARED, fd, 0 );
	if( NULL == base ) {
		close( fd );
		perror( "failed mmap'ing target file" );
		return -1;
	}

	/**
	  * Allocate a response buffer.
	  */

	offsets = (long*)calloc( g_request_buf_len, sizeof(long) );
	if( NULL == offsets ) {
		munmap( (void*)base, info.st_size );
		close( fd );
		return -1;
	}

	/**
	  * Notice the strategy below: We immediately convert each requested to
	  * key to its offset(s) in the index--that is, to a response value, so 
	  * there's no need to buffer requests...only responses.
	  * If ASCII EOT (0x04) is anywhere in the stream, exit server mode.
	  */

	while( not eot ) {

		assert( n_req == 0 );

		// Accept requests

		fp = fopen( fifoname, "r" );
		if( fp ) {
			int from, to;
			while( not feof( fp ) 
					and buf == fgets( buf, MAXLEN_KEY+1, fp )) {

				/**
				  * Log exactly what was received. This is primarily for
				  * debugging arcane Unicode/ASCII and line termination
				  * issues, so don't alter anything...
				  */
				if( arg_verbosity >= VERB_DEBUG ) 
					fprintf( stdout, "read '%s'\n", buf );

				/**
				  * EOT may be -anywhere- in the stream. In particular, it
				  * might immediately follow a valid token, so just let 
				  * token parsing (below) consume everything valid -then-
				  * check for EOT.
				  */

				to = 0; // Both lookup_* functions return -1 if lookup fails
				if( arg_numeric_keys ) {
					char *pc;
					const int key = (int)strtol( buf, &pc, 0 );
					// strtol will consume all valid digits, so...
					eot = (*pc == 0x04); // Was a 'terminate' char embedded?
					// *pc is typically a NL. Though this won't have hurt
					// strtol's operation, replace NL with normal NULL
					// terminator, so that 'not found' log messages below
					// are on a single line.
					*pc = 0;
					from = pc > buf ? lookup_num( key, index_len, &to ) : -1;
				} else {
					// Whitespace is implicitly ignored by strtol, but
					// can foul things up here. In particular, make sure
					// we chop NL off of buf that fgets leaves in place!
					char *pc = buf;
					buf[MAXLEN_KEY] = 0; // defensive
					while( isgraph(*pc) ) pc++;
					eot = (*pc == 0x04); // Was a 'terminate' char embedded?
					// Replace whatever non-printable char terminated the 
					// key with a normal null-termination...
					*pc = 0;
					from = pc > buf ? lookup_str( buf, index_len, &to ) : -1;
				}

				if( eot ) {
					if( arg_verbosity >= VERB_INFO ) 
						fprintf( stdout, "received ASCII EOT, exiting...\n" );
					break;
				}

				/**
				  * Grow the response buffer if necessary.
				  * Note even on missing key (to-from) == 1 so that an 
				  * 'missing key' response can be given.
				  */

				if( not ( n_req + ( to - from ) <= g_request_buf_len ) ) {
					const int count 
						= __max( n_req+(to-from), 2*g_request_buf_len );
					void *pv = realloc( offsets, count * sizeof(long) );
					if( pv ) {
						offsets = (long*)pv;
						g_request_buf_len = count;
					} else {
						fprintf( stderr, 
							"failed increasing request buffer size."
							"...proceeding but ignoring requests after #%d\n", 
							g_request_buf_len );
						// TODO: Insert warning in output data stream, too?
						break;
					}
				}

				/**
				  * Convert the request(s) to file offset(s)...
				  */

				if( from < 0 ) {
					if( arg_verbosity >= VERB_INFO )
						fprintf( stdout, "'%s' not found\n", buf );
					if( opt_output_empty_lines
					 	and n_req < g_request_buf_len ) 
						offsets[n_req++] = -1;
					continue;
				}

				if( arg_numeric_keys ) {
					while( from < to and n_req < g_request_buf_len ) 
						offsets[n_req++] = g_index.num[from++].offset;
				} else {
					while( from < to and n_req < g_request_buf_len )
						offsets[n_req++] = g_index.str[from++].offset;
				}
			}
			fclose( fp );
		} else {
			perror( "failed opening fifo for input" );
			break;
		}

		if( n_req == 0 ) continue;

		/**
		  * Sort the offsets for efficient file access. Note that this
		  * really only "optimizes" if a significant number of requests,
		  * are, in fact, clustered in the file. Otherwise, it could
		  * actually detract.
		  */

		if( arg_optimize ) {
			qsort( offsets, n_req, sizeof(long), __cmp_foffset );
		}

		// And dump them out

		fp = fopen( fifoname, "w" );
		if( fp ) {

			/**
			  * Though it's -slightly- more efficient to decrement, the
			  * the possibility that user wants request order preserved 
			  * requires we increment...
			  */
			int r = 0;
			while( r < n_req ) {
				if( offsets[r] < 0 ) { // indicates missing key
					fputs( "\n", fp );
				} else {
					const char *pc = base + offsets[r];
					do { fputc( *pc, fp ); } while( '\n' != *pc++ );
				}
				r++;
			}
			fclose( fp );
		} else {
			perror( "failed opening fifo for output" );
			break;
		}
		n_req = 0;
	}

	/**
	  * Cleanup
	  */

	free( offsets );
	munmap( (void*)base, info.st_size );
	close( fd );

	return 0;
}


static int count_lines( const char *filename ) {

	int lc = 0,cc;
	int count = 0;
	FILE *fp = fopen( filename, "r");
	if( fp ) {
		do {
			cc = fgetc(fp);
			if( '\n' == cc ) 
				count++;
			else
			if( EOF == cc ) 
				break;
			lc = cc;
		} while( not feof(fp) );
		fclose( fp );
		// If the final line was not '\n'-terminated, bump the line 
		// count to account for the last line.
		if( '\n' != lc ) count++;
	} else {
		// ...it'll fail again below. Deal with it there.
	}
	return count;
}


static const char *USAGE = 
"%s [ -O -n -q ] [ -v <log verbosity> ] [ -c <col> ] [ -f <fifoname> ] [ -l <count> ] [ -h <count> ] <file>\n"
"-v verbosity: 0=silent, 1=info, 2=debug (reduces performance), default:%d\n"
"-O optimize (Implies that requests can be reordered.)\n"
"-n keys are numeric (Otherwise, keys are strings <= %d chars.)\n"
"-q Don't respond with empty lines for missing keys(\"quiet mode\").\n"
"   Only use this if you are certain you will never pass invalid\n"
"   keys since a process (including the Python wrapper) reading\n"
"   from the fifo may block indefinitely awaiting response.\n"
"-c Index column number <col> (leftmost is #1, not 0.) (default: %d)\n"
"-f use <fifoname> for all IPC\n"
"-l target file contains <= <count> data lines. Else, file is scanned.\n"
"-h skip <count> (presumably header) lines when indexing (default: %d)\n"
"All boolean options default to the opposite state described above.\n"
"The fifo name specified on the command line overrides that in the\n"
"environment variable '%s'.\n";

/**
  * Print usage -with hardcoded defaults- from code.
  */
static void print_usage( const char *exename, FILE *fp ) {
	fprintf( stderr, USAGE, 
			exename, 
			arg_verbosity,
			MAXLEN_KEY, 
			arg_column,
			arg_skip,
			NAMED_PIPE );
}

static const char *OPTIONS = 
		"v:Onqc:f:l:h:V";

int main( int argc, char *argv[] ) {

	int index_len = 0;
	struct stat info;
	/**
	  * Unless empty lines are emitted client may block waiting for output
	  * that won't arrive (if invalid key was sent.
	  */
	const char *arg_filename = NULL;
	FILE *fp = NULL;
	char c;

	/**
	  * Global initializations and validations.
	  */

	memset( arg_fifoname, 0, sizeof(arg_fifoname) );
	g_index.num  = NULL;
	g_one_to_one = true; // until one of __cmp_'s show otherwise

	assert( sizeof(long) == 8 );

	if( argc < 2 ) {
		print_usage( argv[0], stderr );
		exit(-1);
	}

	c = getopt( argc, argv, OPTIONS );
	while( c != -1 ) {
		switch (c) {
		case 'v':
			arg_verbosity = strtol( optarg, NULL, 0 );
			break;

		case 'O':
			arg_optimize = true;
			break;

		case 'n':
			arg_numeric_keys = true;
			break;

		case 'q':
			arg_emit_empty_lines = false;
			break;

		case 'c':
			arg_column = strtol( optarg, NULL, 0 );
			break;

		case 'f':
			// If a FIFO name was provided on the comman line it overrides the 
			// environment. Otherwise, check the environment...
			strcpy( arg_fifoname, optarg );
			break;

		case 'l':
			arg_n_lines = strtol( optarg, NULL, 0 );
			break;

		case 'h':
			arg_skip = strtol( optarg, NULL, 0 );
			break;

		case 'V':
			fprintf( stdout, "%s compiled on %s %s\n", argv[0], __DATE__, __TIME__ );
			exit(0);
			break;

		case '?':
			print_usage( argv[0], stderr );
			exit(0);
			break;

		default:
			fprintf( stderr, "# fail: unknown option '%c'\n", c );
			print_usage( argv[0], stderr );
			exit(-1);
		}
		c = getopt( argc, argv, OPTIONS );
	}

	if( arg_verbosity >= VERB_DEBUG ) {
		fprintf( stdout, "Command line (possibly permuted):\n" );
		for(int i = 0; i < argc; i++ ) 
			fprintf( stdout, "%02d: %s\n", i, argv[i] );
	}

	/**
	  * Verify required arguments are present and valid.
	  */

	if( strlen( arg_fifoname ) == 0 ) {
		if( getenv( NAMED_PIPE ) ) {
			strncpy( arg_fifoname, getenv( NAMED_PIPE ), FILENAME_MAX );
		} else {
			fprintf( stderr, "No fifo specified (in cmd line or environment).\n" );
			exit(-1);
		}
	}

	if( not ( optind < argc ) ) {
		fprintf( stderr, "No file specified for indexing.\n" );
		exit(-1);
	}

	arg_filename = argv[ optind ];

	if( stat( arg_filename, &info ) ) {
		fprintf( stderr, "failed opening target file: '%s'\n", arg_filename );
		perror( "opening input file" );
		exit(-1);
	}

	/**
	  * If a line count was specified, trust it. Otherwise pre-scan the file
	  * to count lines.
	  */

	if( arg_n_lines == 0 ) arg_n_lines = count_lines( arg_filename );

	// Report the parameters we're using. Just a sanity check.

	if( arg_verbosity > VERB_SILENT ) {
		fprintf( stdout, "Table has %d data lines.\n", 
				arg_n_lines );
	}

	/**
	  * Build the index and start serving...
	  */

	fp = fopen( arg_filename, "r");
	if( fp ) {
		int err = setjmp( context );
		if( err ) {
			fprintf( stderr, "longjmp from source line %d during indexing.\n", err );
		} else {

			if( arg_verbosity > VERB_SILENT ) {
				fprintf( stdout, "indexing column %d of %s (skipping %d lines)...\n", 
						arg_column, arg_filename, arg_skip );
			}

			/**
			  * Create the index array itself.
			  */
			if( arg_numeric_keys ) {
				g_index.num = (numkey_t*)calloc( arg_n_lines, sizeof(numkey_t) );
			} else {
				g_index.str = (strkey_t*)calloc( arg_n_lines, sizeof(strkey_t) );
			}

			index_len = mkindex( fp, arg_skip, arg_column, arg_n_lines );

			if( arg_verbosity > VERB_SILENT ) {
				fprintf( stdout, "Indexed %d lines.\n", 
						index_len );
				fprintf( stdout, "Keys are %sinjective.\n", 
						g_one_to_one ? "" : "not " );
				fprintf( stdout, "Entering server mode...\n...%spreserving request order.\n", 
						arg_optimize ? "not " : "" );
				fprintf( stdout, "...%sreporting missing identifiers as empty lines.\n", 
						arg_emit_empty_lines ? "" : "not " );
				fprintf( stdout, "...verbosity: %d.\n", 
						arg_verbosity );
				fflush( stdout ); // in case user is waiting for above status
			}
		}
		fclose( fp );

		err = setjmp( context );
		if( err ) {
			fprintf( stderr, "longjmp from source line %d during serve(...).\n", err );
		} else {
			serve( 
				arg_filename, 
				arg_fifoname, 
				index_len,
				arg_emit_empty_lines );
		}
	} else {
		perror( "opening file" );
	}
	if( g_index.num ) free( g_index.num );
}

