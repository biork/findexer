
import sys
sys.path.append( '/local/dev/my/py3k' )
import indexed
import gc

targ = None
fifo = None

if len(sys.argv) < 2:
	sys.stderr.write( sys.argv[0]+"{ target file | fifo }\n" )
	sys.exit(-1)
if sys.argv[1].endswith('fifo'):
	fifo = sys.argv[1]
else:
	targ = sys.argv[1]

table = indexed.File( filename=targ, fifoname=fifo, numeric=False, column=2, skip=1 )
try:
	sought = 'red'
	print( "Fetch:", sought )
	for l in table.fetch( sought ):
		sys.stdout.write( l )
	sought = ['blue','tan']
	print( "Fetch:", sought )
	for l in table.fetch( sought ):
		sys.stdout.write( l )
	sought = ['purple','gray']
	print( "Fetch:", sought )
	for l in table.fetch( sought ):
		sys.stdout.write( l )
#except LookupError as x:
except LookupError, x:
	sys.stderr.write( 'color lookup(s) failed\n' )
	sys.stderr.write( str(x) )
	sys.stderr.write( '\n'   )
table = None
gc.collect()


table = indexed.File( filename=targ, fifoname=fifo, numeric=True, column=1, skip=1 )
try:
	sought = '2032'
	print( "Fetch:", sought )
	for l in table.fetch( '2032' ):
		sys.stdout.write( l )
	sought = ['5288','8348']
	print( "Fetch:", sought )
	for l in table.fetch( sought ):
		sys.stdout.write( l )
	sought = ['123','456']
	print( "Fetch:", sought )
	for l in table.fetch( sought ):
		sys.stdout.write( l )
#except LookupError as x:
except LookupError, x:
	sys.stderr.write( 'numeric lookup(s) failed\n' )
	sys.stderr.write( str(x) )
	sys.stderr.write( '\n'   )
table = None
gc.collect()

